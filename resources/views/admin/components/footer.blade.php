 <!-- Control Sidebar -->
 <aside class="control-sidebar control-sidebar-dark">
     <!-- Control sidebar content goes here -->
 </aside>
 <!-- /.control-sidebar -->
 <!-- Main Footer -->
 <footer class="main-footer">
     {{-- <strong>Copyright &copy; 2014-2021 <a href="https://adminlte.io">AdminLTE.io</a>.</strong>
    All rights reserved. --}}
     <div class="float-right d-none d-sm-inline-block">
         <b>Version</b> 0.0.1
     </div>
 </footer>
 </div>
 <!-- ./wrapper -->
 <!-- REQUIRED SCRIPTS -->
 <!-- jQuery -->
 <script src="{{ asset('assets/plugins/jquery/jquery.min.js') }}"></script>
 <!-- Bootstrap -->
 <script src="{{ asset('assets/plugins/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
 <!-- overlayScrollbars -->
 <script src="{{ asset('assets/plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js') }}"></script>
 <!-- AdminLTE App -->
 <script src="{{ asset('assets/dist/js/adminlte.js') }}"></script>
 <!-- DataTables  & Plugins -->
 <script src="https://cdn.datatables.net/1.10.25/js/jquery.dataTables.min.js"></script>
 <script src="https://cdn.datatables.net/1.10.25/js/dataTables.bootstrap4.min.js"></script>
 <script src="https://cdn.datatables.net/autofill/2.3.7/js/dataTables.autoFill.min.js"></script>
 <script src="https://cdn.datatables.net/autofill/2.3.7/js/autoFill.bootstrap4.min.js"></script>
 <!-- InputMask -->
 <script src="{{ asset('assets/plugins/moment/moment.min.js') }}"></script>
 <!-- Tempusdominus Bootstrap 4 -->
 <script src="{{ asset('assets/plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js') }}"></script>

 <script type="text/javascript">
     $(document).ready(function() {
         $('#datatable').DataTable({
             processing: true,
             serverside: true,
             ajax: {
                 url: "{{ route('Admin.users.index') }}",
                 type: 'GET'
             },
             columns: [{
                     data: 'DT_RowIndex',
                     name: 'DT_RowIndex',
                     searchable: false
                 },
                 {
                     data: 'username',
                     name: 'username'
                 },
                 {
                     data: 'activationAccount',
                     name: 'activationAccount'
                 },
                 {
                     data: 'createdAt',
                     name: 'create'
                 },
                 {
                     data: 'formattedDate',
                     name: 'formattedDate'
                 },
                 {
                     data: 'status',
                     name: 'status'
                 },
                 {
                     data: 'action',
                     name: 'action',
                     orderable: false
                 }
             ],
         });

         var id = $('#datatable_shopee_account').attr("data-id");
         var url = '{{ route('Admin.users.detail', ':id') }}'
         url = url.replace(':id', id);


         $('#datatable_shopee_account').DataTable({


             processing: true,
             serverside: true,
             ajax: {
                 url: "",
                 type: 'GET'
             },
             columns: [{
                     data: 'DT_RowIndex',
                     name: 'DT_RowIndex',
                     searchable: false
                 },
                 {
                     data: 'shopee_account',
                     name: 'shopee_account'
                 },
                 {
                     data: 'action',
                     name: 'action',
                     orderable: false
                 }
             ],
         });
     })
 </script>
 <script type="text/javascript">
     function formatDate(date) {
         var d = new Date(date),
             month = '' + (d.getMonth() + 1),
             day = '' + d.getDate(),
             year = d.getFullYear();

         if (month.length < 2)
             month = '0' + month;
         if (day.length < 2)
             day = '0' + day;

         return [year, month, day].join('-');
     };


     $(function() {
         $('#expiredate').datetimepicker({
             defaultDate: Date.now(),
             minDate: Date.now(),
         });
     });


     $(function() {
         var date = "{{ isset($user_data) ? $user_data->exp_date : '' }}";
         $('#editdate').datetimepicker({
             defaultDate: formatDate(date),
             minDate: formatDate(date),
         });
     });

     const password = document.querySelector('#password');
     const eye = document.querySelector('#eye')
     const togglePassword = document.querySelector('#togglePassword');

     togglePassword?.addEventListener('click', function(e) {
         // toggle the type attribute
         const type = password.getAttribute('type') === 'password' ? 'text' : 'password';
         password.setAttribute('type', type);
         // toggle the eye / eye slash icon
         eye.classList.toggle('fa-eye');
         if (eye.classList.contains('fa-eye')) {
             eye.classList.remove('fa-eye-slash')
         } else {
             eye.classList.add('fa-eye-slash')
         }
     });
 </script>
 </body>

 </html>
