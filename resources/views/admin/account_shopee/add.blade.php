@include('admin.components.header')
@include('admin.components.navbar')
@include('admin.components.sidebar')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0">Add Account Shopee</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{ url('dashboard') }}">Home</a></li>
                        <li class="breadcrumb-item"><a href="{{ route('Admin.users.detail', $customer->user_id) }}">
                                Detail
                                User Management</a></li>
                        <li class="breadcrumb-item active">Add Account</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="card">
                <form action="{{ route('Admin.users_shopee.store_account') }}" method="POST">
                    @csrf
                    <div class="card-body">

                        <div class="form-group">
                            <label for="username">Username</label>
                            <input type="text" readonly
                                class="form-control {{ $errors->has('username') ? 'is-invalid' : '' }}"
                                name="username" id="username" placeholder="Enter username"
                                value="{{ $customer->username }}">
                            @if ($errors->has('username'))
                                <div class="invalid-feedback">
                                    {{ $errors->first('username') }}
                                </div>
                            @endif
                        </div>
                        <div class="form-group">
                            <label for="shopee_account">Shopee Account Name</label>
                            <input type="text"
                                class="form-control {{ $errors->has('shopee_account') ? 'is-invalid' : '' }}"
                                name="shopee_account" id="shopee_account" placeholder="Enter Shopee Account Name"
                                value="" required>
                            @if ($errors->has('shopee_account'))
                                <div class="invalid-feedback">
                                    {{ $errors->first('shopee_account') }}
                                </div>
                            @endif
                        </div>
                    </div>
                    <div class="d-block px-4 mb-4">
                        <button type="submit" class="btn btn-primary btn-block">Save</button>
                    </div>
                </form>
            </div>
        </div>
        <!--/. container-fluid -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
<!-- Date -->
@include('admin.components.footer')
