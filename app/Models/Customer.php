<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    use HasFactory;

    protected  $primaryKey = 'user_id';

    protected $table = 'user';

    protected $fillable = [
        'username', 'password', 'exp_date', 'reg_date', 'jumlah_user'
    ];

    protected $hidden = ['password'];

    public $timestamps = false;
}
