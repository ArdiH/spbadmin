<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Customer;

class DashboardController extends Controller
{
    public function index()
    {
        $countAll = Customer::count();
        $countExpired = Customer::where('exp_date', '>=', date('Y-m-d'))->count();
        $data = [
            'all' => $countAll,
            'expired' => $countExpired
        ];
        return view('admin.dashboard.index', compact('data'));
    }
}
